import sys
from typing import List


def calc(expr: str) -> int:
    a, op, b = expr.split()
    a = int(a)
    b = int(b)
    if op == '+':
        return a + b
    elif op == '-':
        return a - b
    elif op == '*':
        return a * b
    elif op == '/':
        return a // b
    else:
        raise Exception(f"Unknown operator {op!r}.")


def main(argv: List[str]):
    if argv:
        expr = ' '.join(argv)
    else:
        expr = input("calc: ")
    ans = calc(expr)
    print(ans)


if __name__ == '__main__':
    main(sys.argv[1:])
