import pytest

from calc import calc


@pytest.mark.parametrize('expr, ans', [
    ('2 + 2', 4),
    ('2 - 2', 0),
    ('2 * 2', 4),
    ('2 / 2', 1),
    ('9 / 3', 3),
    ('10 / 3', 3),

])
def test_calc(expr, ans):
    assert calc(expr) == ans


def test_calc_div_zero():
    with pytest.raises(ZeroDivisionError):
        calc('2 / 0')


def test_calc_unknown_operator():
    with pytest.raises(Exception) as e:
        calc('2 ? 2')
    assert str(e.value) == "Unknown operator '?'."
