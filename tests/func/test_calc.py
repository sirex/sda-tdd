from typing import Callable

import pytest

from calc import main


@pytest.fixture()
def calc(mocker) -> Callable:
    mock = mocker.patch('builtins.print')
    return lambda: mock.call_args[0][0]


@pytest.mark.parametrize('expr, ans', [
    ('2 + 2', 4),
    ('2 - 2', 0),
    ('2 * 2', 4),
    ('2 / 2', 1),
])
def test_main(calc, expr, ans):
    main(expr.split())
    assert calc() == ans


@pytest.mark.parametrize('expr, ans', [
    ('2 + 2', 4),
    ('2 - 2', 0),
    ('2 * 2', 4),
    ('2 / 2', 1),
])
def test_main_input(mocker, calc, expr, ans):
    mocker.patch('builtins.input', return_value=expr)
    main([])
    assert calc() == ans
